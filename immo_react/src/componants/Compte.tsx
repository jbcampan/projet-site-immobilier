import React, { useState } from "react";

export default function Compte() {

    const [selectedButton, setSelectedButton] = useState("login")

    return (

        <div className="bg-white p-3">
            <div>
                <button type="button" className={`btn ${selectedButton == 'login' ? ' btn-primary' : ' btn-secondary'}`} onClick={() => setSelectedButton('login')}>Se connecter</button>
                <button type="button" className={`btn ${selectedButton == 'sign in' ? ' btn-primary' : ' btn-secondary'}`} onClick={() => setSelectedButton('sign in')}>Créer un compte</button>
            </div>
            <form action="" method="">
                <div className="form-group">
                    <label htmlFor="InputEmail1" className="form-label mt-4">Adresse Email</label>
                    <input type="email" className="form-control" id="InputEmail1" aria-describedby="emailHelp" placeholder="Entrez votre adresse Email" />
                    <small id="emailHelp" className="form-text text-muted">Nous ne partagerons pas votre adresse.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="Password" className="form-label mt-4">Mot de passe</label>
                    <input type="password" className="form-control" id="Password" placeholder="Mot de passe" />
                </div>
                <div className={`form-group ${selectedButton == 'login' ? ' d-none' : ''}`}>
                    <label htmlFor="Password2" className="form-label mt-4">Confirmez votre mot passe</label>
                    <input type="password" className="form-control" id="Password2" placeholder="Confirmer le mot de passe" />
                </div>
                <div className="d-flex justify-content-center">
                    <button type="button" className="btn btn-danger mt-3">Confirmer</button>
                </div>
            </form>

        </div>

    )
}