import React from "react"
import { NavLink } from 'react-router-dom'


export default function Navbar() {

  return (
    <div className="fixed-top">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <NavLink to="/" className="nav-link">
            <img className="logo px-3" src="/src/assets/img/logo.png" alt="" />
          </NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarColor03">
            <ul className="navbar-nav me-auto">
              <li className="nav-item">
                <NavLink to="/biens/achat" className="nav-link">Acheter</NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/biens/location" className="nav-link">Louer</NavLink>
              </li>
            </ul>
            <form className="d-flex">
              <input className="form-control me-sm-2" type="search" placeholder="Rechercher" />
              <button className="btn btn-secondary my-2 my-sm-0" type="submit">Rechercher</button>
            </form>
            <NavLink to="/compte" className="nav-link">Mon compte</NavLink>
          </div>
        </div>
      </nav>
    </div>
  )
}