import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import config from "../../config.json";
import Bien from "./Bien";

interface Bien {
    id: number;
    type_bien: TypeBien;
    type_transaction: TypeTransaction;
    lieu_affiche: string;
    resume: string;
    prix: number;
    ville: string;
    codePostal: string;
    photos: any[];
    proprietaire: any;
}

interface TypeBien {
    intitule: string;
}

interface TypeTransaction {
    intitule: string;
}

export default function Biens() {

    const [biens, setBiens] = useState<Bien[]>([]);
    const [typesBien, setTypesBien] = useState<any>([]);
    const [typesTransaction, setTypesTransaction] = useState<any>([]);
    const [filtreLieux, setFiltreLieux] = useState<any>([]);
    const [filtreTransac, setFiltreTransac] = useState("Type de transaction ");
    const [filtreBien, setFiltreBien] = useState("");
    const [filtrePrix, setFiltrePrix] = useState("")



    useEffect(() => {
        fetch(`${config.api}/api/biens`)
            .then(response => response.json())
            .then(biens => setBiens(biens['hydra:member']))
            .catch(error => console.error(error));
    }, []);


    // console.log(biens);
    // console.log(filtreLieux);
    // console.log(filtrePrix);




    useEffect(() => {
        fetch(`${config.api}/api/types_biens`)
            .then(response => response.json())
            .then(typesBien => setTypesBien(typesBien['hydra:member']))
            .catch(error => console.error(error));
    }, []);

    // console.log(typesBien);

    useEffect(() => {
        fetch(`${config.api}/api/types_transactions`)
            .then(response => response.json())
            .then(typesTransaction => setTypesTransaction(typesTransaction['hydra:member']))
            .catch(error => console.error(error));
    }, []);


        const { filter } = useParams();
        let filteredBiens;

        switch (filter) {
            case 'achat':
                filteredBiens = biens.filter((bien) => bien.type_transaction.intitule === 'Vendre');
                break;
            case 'location':
                filteredBiens = biens.filter((bien) => bien.type_transaction.intitule === 'Louer');
                break;
            default:
                filteredBiens = biens;
        }


        const handleSubmit = (e) => {
            console.log(typesTransaction);

        }

        return (
            <div className="bg-custom-light-grey">
                <div className="nav-space"></div>
                <div className="text-center">
                    <h1 className="lead py-4 bg-dark text-white">{filteredBiens.filter(bien => bien.type_bien.intitule === filtreBien).length} Annonces correspondent à votre recherche</h1>
                </div>
                <div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-11 offset-1">
                            <p>Trier par : </p>
                        </div>
                        <div className="col-7 offset-1">

                            {filteredBiens.filter(bien => (filtreBien != "" ? bien.type_bien.intitule === filtreBien : true)).filter(bien => (filtreLieux != "" ? bien.lieu_affiche === filtreLieux : true)).filter(bien => (filtrePrix != "" ? bien.prix >= filtrePrix : true)).map((bien) =>
                                <Bien bien={bien} key={bien.id} />
                            )}

                        </div>
                        <div className="col-3 vh-100 recherche-bien">
                            <div className="col-10 offset-1">
                                <form action="/biens" className="bg-white bg-opacity-25 p-3">
                                    <div className="form-floating ">
                                        <select name="type_transaction" id="type_transaction" className="form-select" onChange={(e) => {
                                            setFiltreTransac(e.target.value);
                                        }}>
                                            {typesTransaction.map((type_transaction) =>
                                                <option value={type_transaction.intitule} key={type_transaction.id}>{type_transaction.intitule}</option>
                                            )}
                                        </select>
                                        <label htmlFor="type_transaction">{filtreTransac}</label>
                                    </div>
                                    <div className="form-floating">
                                        <select name="type_bien" id="type_bien" className="form-select" onChange={(e) => { setFiltreBien(e.target.value) }}>
                                            <option >Select a type</option>

                                            {typesBien.map((type_bien) =>
                                                <option value={type_bien.intitule} key={type_bien.id}>{type_bien.intitule}</option>
                                            )}
                                        </select>
                                        <label htmlFor="type_bien">Type&nbsp;de&nbsp;bien {filtreBien}</label>
                                    </div>
                                    <div className="form-floating" >
                                        <input className="form-control" type="text" id="lieu_affiche" name="lieu_affiche" onChange={(e) => { setFiltreLieux(e.target.value) }} />
                                        <label htmlFor="lieu_affiche">Lieu</label>
                                    </div>
                                    <div className="form-floating">
                                        <input className="form-control" type="text" id="prix" name="prix" onChange={(e) => { setFiltrePrix(e.target.value) }} />
                                        <label htmlFor="prix">Prix max</label>
                                    </div>
                                    <button onClick={e => { e.preventDefault(); handleSubmit(e); }} className="btn btn-outline-primary w-100">Rechercher</button>
                                </form>

                                {/* 
                                <div className="form-floating ">
                                    <select name="type_transaction" id="type_transaction" className="form-select">
                                        {typesTransaction.map((type_transaction) =>
                                            <option value={type_transaction.id} key={type_transaction.id}>{type_transaction.intitule}</option>
                                        )}
                                    </select>
                                    <label htmlFor="type_transaction">Type&nbsp;d'offre</label>
                                </div>
                                <div className="form-floating">
                                    <select name="type_bien" id="type_bien" className="form-select">
                                        {typesBien.map((type_bien) =>
                                            <option value={type_bien.id} key={type_bien.id}>{type_bien.intitule}</option>
                                        )}
                                    </select>
                                    <label htmlFor="type_bien">Type&nbsp;de&nbsp;bien</label>
                                </div>
                                <div className="form-floating">
                                    <input className="form-control" type="text" id="lieu_affiche" name="lieu_affiche" />
                                    <label htmlFor="lieu_affiche">Lieu</label>
                                </div>
                                <div className="form-floating">
                                    <input className="form-control" type="text" id="prix" name="prix" />
                                    <label htmlFor="prix">Prix max</label>
                                </div> */}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }