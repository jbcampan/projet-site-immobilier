import React, { useState, useEffect } from 'react';
import Bien from "./Bien";
import config from "../../config.json";

interface Bien {
    id: number;
    type_bien: TypeBien;
    type_transaction: TypeTransaction;
    lieu_affiche: string;
    resume: string;
    prix: number;
    ville: string;
    codePostal: string;
    photos: any[];
    proprietaire: any;
}

interface TypeBien {
    intitule: string;
}

interface TypeTransaction {
    intitule: string;
}

export default function Client() {

    const [biens, setBiens] = useState<Bien[]>([]);

    useEffect(() => {
        fetch(`${config.api}/api/biens`)
            .then(response => response.json())
            .then(biens => setBiens(biens['hydra:member']))
            .catch(error => console.error(error));
    }, []);


    console.log(biens);


    return (
        <>
            <div className="nav-space"></div>
            <div className="text-center">
                <h1 className="lead py-4 bg-dark text-white ">Gérez vos {biens.length} biens.</h1>
            </div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3 bg-dark">
                        <p className='d-flex'>ppp</p>
                    </div>
                    <div className="col-9">
                        {biens.map((bien) =>
                            <Bien bien={bien} key={bien.id} />
                        )}
                    </div>
                </div>
            </div>

            
        </>
    )
}