import React from 'react';
import { NavLink } from "react-router-dom";
import config from "../../config.json";


export default function Bien({ bien }) {

    return (
        <div className="card mb-3 card-custom overflow-hidden" key={bien.id}>
            <div className="row g-0">
                <div className="col-md-6">
                    <img src={`${config.api}/img/${bien.photos[0].fichier}`} className="img-fluid" alt="..." />
                </div>
                <div className="col-md-6">
                    <div className="card-body">
                        <h6 className="card-title">{bien.type_bien.intitule} à {bien.type_transaction.intitule}</h6>
                        <h6 className="card-subtitle text-muted pb-3">{bien.lieu_affiche}</h6>
                        <h4 className="card-title"><span className="badge rounded-pill bg-light border-danger text-danger">{bien.prix} &euro; </span></h4>
                        <p className="card-text">{bien.resume}</p>
                        <NavLink to={`/bien/detail/${bien.id}`}><button className="btn btn-danger" >Voir l'annonce</button></NavLink>
                    </div>
                </div>
            </div>
        </div>
    )
}