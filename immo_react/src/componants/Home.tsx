import React, { useState, useEffect } from 'react';
import config from '../../config.json'
import { useParams } from 'react-router-dom';
import Compte from './Compte';

let nombre = Math.ceil(Math.random() * 3);

export default function Home() {

    const [typesBien, setTypesBien] = useState<any>([]);
    const [typesTransaction, setTypesTransaction] = useState<any>([]);
    const [lieux, setLieux] = useState<any>([]);


    useEffect(() => {
        fetch(`${config.api}/api/types_biens`)
            .then(response => response.json())
            .then(typesBien => setTypesBien(typesBien['hydra:member']))
            .catch(error => console.error(error));
    }, []);

    // console.log(typesBien);

    useEffect(() => {
        fetch(`${config.api}/api/types_transactions`)
            .then(response => response.json())
            .then(typesTransaction => setTypesTransaction(typesTransaction['hydra:member']))
            .catch(error => console.error(error));
    }, []);

    // console.log(typesBien);

    const { filter } = useParams();
    let filteredForm;

    switch (filter) {
        case 'compte':
            filteredForm =
                <Compte />
            break;

        default:
            filteredForm =
                <form action="/biens" className="bg-white bg-opacity-25 p-3" onSubmit={e => this.handleSubmit(e)}>
                    <div className="d-flex">
                        <div className="form-floating ">
                            <select name="type_transaction" id="type_transaction" className="form-select">
                                {typesTransaction.map((type_transaction) =>
                                    <option value={type_transaction.id} key={type_transaction.id}>{type_transaction.intitule}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                )}
                            </select>
                            <label htmlFor="type_transaction">Type&nbsp;d'offre</label>
                        </div>
                        <div className="form-floating">
                            <select name="type_bien" id="type_bien" className="form-select">
                                {typesBien.map((type_bien) =>
                                    <option value={type_bien.id} key={type_bien.id}>{type_bien.intitule}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                )}
                            </select>
                            <label htmlFor="type_bien">Type&nbsp;de&nbsp;bien</label>
                        </div>
                        <div className="form-floating">
                            <input className="form-control" type="text" id="lieu_affiche" name="lieu_affiche" />
                            <label htmlFor="lieu_affiche">Lieu</label>
                        </div>
                        <div className="form-floating">
                            <input className="form-control" type="text" id="prix" name="prix" />
                            <label htmlFor="prix">Prix max</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Rechercher</button>
                    </div>
                </form>
    }

    return (
        <div>
            <div className="vh-100 main-img position-relative" style={{ backgroundImage: `url("/src/assets/img/home${nombre}.jpg")` }}>
                <div className="position-absolute top-50 start-50 translate-middle">
                    {filteredForm}
                </div>
                <div className="bg-white position-absolute bottom-0 start-50 translate-middle-x w-100 bobo text-center d-flex justify-content-center align-item-center">
                    <p className="pt-1">Trouvez votre chez vous les boys</p>
                </div>
            </div>
        </div>
    )
}