import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import config from '../../config.json'

export default function DetailBien({ }) {

  const [bien, setBien] = useState<any>({});
  const [views, setViews] = useState(bien.nombre_vues);
  const { id } = useParams();

  useEffect(() => {
    fetch(`${config.api}/api/biens/${id}`)
      .then(response => response.json())
      .then(biens => setBien(biens))
      .catch(error => console.error(error));

    fetch(`${config.api}/api/biens/${id}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ id: id })
    })
      .then(response => {
        console.log(response);
        return response.json()
          ;
      })
      .then(data => {
        console.log(data.views);

        setViews(data.views);

      })
      .catch(error => {
        console.error(error);
      });

  }, []);

  console.log(bien);

  // console.log(views);


  return (
    <>

      <div className="nav-space"></div>
      <div className="container-fluid bg-primary pt-5 pb-4">
        <div className="row">
          <div className="col-8 offset-2">
            <div id="carouselExampleIndicators" className="carousel slide">
              <div className="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
              </div>
              <div className="carousel-inner carousel-custom">
                {bien.photos && bien.photos.map((photo, i) =>
                  <div className={"carousel-item" + (i === 0 ? ' active' : '')} key={i}>
                    <img src={`${config.api}/img/${photo.fichier}`} className="d-block w-100 img-fluid" alt="..." />
                  </div>
                )}
              </div>
              <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
              </button>
              <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </div>

        <h1 className="p-3 text-white text-center h3 pt-5">{bien.titre} </h1>
      </div>
      
      <div className="container">
        <div className="row">
          <div className="col-6 mt-3 mb-5">
            <h2 className='h4 pt-5 pb-4 text-center'>Description du bien</h2>
            <p className='text-center h5 py-3'>{bien.type_bien?.intitule} à {bien.type_transaction?.intitule} <span className='text-center badge rounded-pill bg-light border-danger text-danger ms-5 fs-5'>{bien.prix} &euro;</span></p>
            <p className='text-center pb-3 text-primary'>{bien.complement_adresse} {bien.rue}, {bien.code_postal} {bien.ville}</p>
            <hr />
            <p className='text-center text-primary'>{bien.description}</p>
          </div>
          <div className="col-6">
          <div id="map"></div>
          </div>
        </div>
      </div>



      {/* <div className="nav-space"></div>
      <section className="container-fluid overflow-auto p-5 bg-primary">
        <div className="row g-3 flex-nowrap" >
          {bien.photos && bien.photos.map((photo, i) =>
            <div className="col-6" key={i}>
              <div className="position-relative img-custom" style={{ backgroundImage: `${config.api}/img/${photo.fichier}` }}>
              </div>
            </div>
          )}
        </div>
        <h1 className="p-3 text-white text-center h3 pt-5">{bien.titre} </h1>

      </section> */}
    </>
  )
}

