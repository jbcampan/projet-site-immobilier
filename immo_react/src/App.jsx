import { useState } from 'react'
import './App.css'
import Header from './componants/Header'
import Home from './componants/Home'

import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Biens from './componants/Biens'
import DetailBien from './componants/DetailBien'
import Compte from './componants/Compte'
import Client from './componants/Client'


function App() {

  return (
    <div className="App">

      <BrowserRouter>
          <Header></Header>      
        <Routes>
          <Route path="/:filter?" element={<Home />}></Route>
          <Route path="/biens/:filter?/" element={<Biens />} />
          <Route path="/bien/detail/:id" element={<DetailBien />} />
          <Route path="/client" element={<Client />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App