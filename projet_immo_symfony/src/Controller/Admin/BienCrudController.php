<?php

namespace App\Controller\Admin;

use App\Entity\Bien;
use Doctrine\ORM\EntityManagerInterface;
// use Symfony\Component\HttpFoundation\JsonResponse;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;


class BienCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Bien::class;
    }
    #[Route('/api/biens/{id}', methods: ['POST'])]
    public function modifier(int $id, EntityManagerInterface $entityManager): string
    {
        $bien = $entityManager->getRepository(Bien::class)->find($id);
        if (!$bien) {
            throw $this->createNotFoundException(
                'No product found for id '. $id
            );
        }

        $bien->setNombreVues($bien.getNombreVues() +1);
        $entityManager->flush();


        // return $this->jsonResponseFactory->create($bien);
        return "du serveur";
    }

    
    public function configureFields(string $pageName): iterable
    {
        
        yield IdField::new('id')->hideOnForm();
        yield TextField::new('titre')->setColumns(5);
        yield TextareaField::new('resume')->setColumns(8);
        yield TextareaField::new('description');
        yield MoneyField::new('prix')->setCurrency('EUR');
        yield TextField::new('rue');
        yield TextField::new('complement_adresse');
        yield TextField::new('ville');
        yield TextField::new('code_postal');
        yield CountryField::new('pays');
        yield TextField::new('lieu_affiche');
        yield IntegerField::new('latitude');
        yield IntegerField::new('longitude');
        yield BooleanField::new('status');
        yield IntegerField::new('nombre_vues');
        yield CollectionField::new('photos');
        yield AssociationField::new('type_bien');
        yield AssociationField::new('type_transaction');
        yield AssociationField::new('user');
        
    }
}
