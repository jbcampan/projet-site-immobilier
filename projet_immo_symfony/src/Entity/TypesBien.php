<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\TypesBienRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TypesBienRepository::class)]
#[ApiResource(
    operations: [
        new Post(normalizationContext: ['groups' => 'property:item']),
        new Get(normalizationContext: ['groups' => 'property:item']),
        new GetCollection(normalizationContext: ['groups' => 'property:list'])
    ]
)]
class TypesBien
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['property:list', 'property:item'])]

    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]

    private ?string $intitule = null;

    #[ORM\OneToMany(mappedBy: 'type_bien', targetEntity: Bien::class, orphanRemoval: true)]
    #[Groups(['property:list', 'property:item'])]

    private Collection $biens;

    public function __toString()
    {
        return $this->getIntitule();
    }

    public function __construct()
    {
        $this->biens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * @return Collection<int, Bien>
     */
    public function getBiens(): Collection
    {
        return $this->biens;
    }

    public function addBien(Bien $bien): self
    {
        if (!$this->biens->contains($bien)) {
            $this->biens->add($bien);
            $bien->setTypeBien($this);
        }

        return $this;
    }

    public function removeBien(Bien $bien): self
    {
        if ($this->biens->removeElement($bien)) {
            // set the owning side to null (unless already changed)
            if ($bien->getTypeBien() === $this) {
                $bien->setTypeBien(null);
            }
        }

        return $this;
    }
}
