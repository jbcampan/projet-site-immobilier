<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\GetCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\BienRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BienRepository::class)]
#[ApiResource(
    operations: [
        new Post(normalizationContext: ['groups' => 'property:item']),
        new Get(normalizationContext: ['groups' => 'property:item']),
        new Put(normalizationContext: ['groups' => 'property:item']),
        new GetCollection(normalizationContext: ['groups' => 'property:list'])
    ]
)]

class Bien
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['property:list', 'property:item'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $titre = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $resume = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $description = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $prix = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $rue = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $complement_adresse = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $ville = null;

    #[ORM\Column(length: 5)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $code_postal = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $pays = null;

    #[ORM\Column(length: 255)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $lieu_affiche = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 12, scale: 9)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $latitude = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 15, scale: 9)]
    #[Groups(['property:list', 'property:item'])]
    private ?string $longitude = null;

    #[ORM\Column]
    #[Groups(['property:list', 'property:item'])]
    private ?bool $status = null;

    #[ORM\Column]
    #[Groups(['property:list', 'property:item'])]
    private ?int $nombre_vues = null;

    #[ORM\OneToMany(mappedBy: 'bien', targetEntity: Photo::class, orphanRemoval: true)]
    #[Groups(['property:list', 'property:item'])]
    private Collection $photos;

    #[ORM\ManyToOne(inversedBy: 'biens')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['property:list', 'property:item'])]
    private ?TypesBien $type_bien = null;

    #[ORM\ManyToOne(inversedBy: 'biens')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['property:list', 'property:item'])]
    private ?TypesTransaction $type_transaction = null;

    #[ORM\ManyToOne(inversedBy: 'biens')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['property:list', 'property:item'])]
    private ?User $user = null;

    public function __toString()
    {
        return $this->getTitre();
    }

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getRue(): ?string
    {
        return $this->rue;
    }

    public function setRue(string $rue): self
    {
        $this->rue = $rue;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complement_adresse;
    }

    public function setComplementAdresse(string $complement_adresse): self
    {
        $this->complement_adresse = $complement_adresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getLieuAffiche(): ?string
    {
        return $this->lieu_affiche;
    }

    public function setLieuAffiche(string $lieu_affiche): self
    {
        $this->lieu_affiche = $lieu_affiche;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function isStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNombreVues(): ?int
    {
        return $this->nombre_vues;
    }

    public function setNombreVues(int $nombre_vues): self
    {
        $this->nombre_vues = $nombre_vues;

        return $this;
    }

    /**
     * @return Collection<int, Photo>
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setBien($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getBien() === $this) {
                $photo->setBien(null);
            }
        }

        return $this;
    }

    public function getTypeBien(): ?TypesBien
    {
        return $this->type_bien;
    }

    public function setTypeBien(?TypesBien $type_bien): self
    {
        $this->type_bien = $type_bien;

        return $this;
    }

    public function getTypeTransaction(): ?TypesTransaction
    {
        return $this->type_transaction;
    }

    public function setTypeTransaction(?TypesTransaction $type_transaction): self
    {
        $this->type_transaction = $type_transaction;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
