<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230316101216 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bien (id INT AUTO_INCREMENT NOT NULL, type_bien_id INT NOT NULL, type_transaction_id INT NOT NULL, titre VARCHAR(255) NOT NULL, resume VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, prix NUMERIC(10, 2) NOT NULL, rue VARCHAR(255) NOT NULL, complement_adresse VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, code_postal VARCHAR(5) NOT NULL, pays VARCHAR(255) NOT NULL, lieu_affiche VARCHAR(255) NOT NULL, latitude NUMERIC(12, 9) NOT NULL, longitude NUMERIC(15, 9) NOT NULL, status TINYINT(1) NOT NULL, nombre_vues INT NOT NULL, INDEX IDX_45EDC38695B4D7FA (type_bien_id), INDEX IDX_45EDC3867903E29B (type_transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, bien_id INT NOT NULL, titre VARCHAR(255) NOT NULL, fichier VARCHAR(255) NOT NULL, INDEX IDX_14B78418BD95B80F (bien_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, civilite VARCHAR(10) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, rue VARCHAR(255) NOT NULL, complement_adresse VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, code_postal VARCHAR(5) NOT NULL, pays VARCHAR(255) NOT NULL, tel_fixe VARCHAR(255) NOT NULL, tel_portable VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38695B4D7FA FOREIGN KEY (type_bien_id) REFERENCES types_bien (id)');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC3867903E29B FOREIGN KEY (type_transaction_id) REFERENCES types_transaction (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418BD95B80F FOREIGN KEY (bien_id) REFERENCES bien (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38695B4D7FA');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC3867903E29B');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418BD95B80F');
        $this->addSql('DROP TABLE bien');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE user');
    }
}
